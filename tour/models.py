from django.db import models


class Client(models.Model):
    first_name = models.CharField(max_length=50, default="")
    second_name = models.CharField(max_length=30, default="", null=True)
    last_name = models.CharField(max_length=50, default="")


class Type(models.Model):
    name = models.CharField(max_length=50)
    cost = models.FloatField(blank=False, null=False)


class Types(models.Model):
    type = models.ManyToManyField(to=Type)
    place_count = models.IntegerField()


class Country(models.Model):
    name = models.CharField(max_length=50)


class Place(models.Model):
    name = models.CharField(max_length=200)
    country = models.ForeignKey(to=Country, on_delete=models.CASCADE)
    type = models.ForeignKey(to=Types, on_delete=models.DO_NOTHING)


class Ticket(models.Model):
    person = models.ForeignKey(to=Client, on_delete=models.CASCADE)
